#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define _GNU_SOURCE         
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/socket.h> 
#include <netinet/in.h> 

#define PORT 8080

#define VTPMO 134 //this depends on what the kernel tells you when mounting the vtpmo module

#define SIZE 4096

unsigned long vtpmo(unsigned long x){
	return syscall(VTPMO,x);
}


#define NUM_TARGET_PAGES 1

char buff[4096*NUM_TARGET_PAGES]__attribute__((aligned(4096)));

int main(int argc, char** argv){
        
       	unsigned long frame_num;
	char c;
	char * buffer;
	int i, sockfd, new_socket;
	struct sockaddr_in address;
	int addrlen = sizeof(address);
        
	buff[0] = 'f';

	buffer = (char *)mmap((void*)0x10000, SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0) ; 
	if (buffer == NULL){
		printf("mmap error\n");
		return -1;	
	}

	buffer[0] = 'f';

       	frame_num = vtpmo((unsigned long)buffer);
	printf("address %p mapped to frame %lu\n",buffer,frame_num);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( PORT ); 

    if (bind(sockfd, (struct sockaddr *)&address, sizeof(address))<0) 
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 

    if (listen(sockfd, 2) < 0) 
    { 
        perror("listen"); 
        exit(EXIT_FAILURE); 
    } 
    if ((new_socket = accept(sockfd, (struct sockaddr *)&address,  
                       (socklen_t*)&addrlen))<0) 
    { 
        perror("accept"); 
        exit(EXIT_FAILURE); 
    } 
    
    if (write(new_socket , &frame_num, sizeof(frame_num)) != sizeof(unsigned long)) {
    	perror("Error in writing in socket");
    	exit(EXIT_FAILURE);
    }

	while(1){
		c = buff[0];
	}

        return 0;
}

